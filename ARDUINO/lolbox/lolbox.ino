#include "Charliplexing.h"
#include "Myfont.h"
#include "Arduino.h"

const int maxChars = 280;
const char endMarker = '\n';

unsigned char displayText[maxChars];
unsigned char rcvText[maxChars];
unsigned char defaultText[] = "LolBox v0.1 \0";
unsigned int state = 0;
unsigned int indexRcv = 0;

void setup()
{
  // initialize serial:
  Serial.begin(19200);
  Serial.setTimeout(5000);
  LedSign::Init();
  //Serial.println("<Arduino is ready>");
  strcpy(displayText, defaultText); // sets default text
}

void loop()
{
    char rc;
    
    if(Serial.available()) {
        if(!state) { //new transmission, reset stuff
            state = 1; // state = started
            indexRcv = 0;
            strcpy(rcvText,"");
            //Serial.println("Reading transmission...");
        }
        
        while(Serial.available() > 0) {
            rc = Serial.read();

            if(rc != endMarker) {
                rcvText[indexRcv] = rc;
                indexRcv++; 
            } else {
                rcvText[indexRcv] = '\0';
                state = 2; // state = finished
                //Serial.println("Finished reading");
            }
        }
    }

    if (state == 2) { // do what you gotta do
        //Serial.println("State Finished, now copying string");
        state = 0;
        strcpy(displayText, rcvText);
    }
    
    Myfont::Banner(strlen(displayText), displayText);
}
