<?php

namespace App\Providers;

use Heidilabs\SerialDriver;
use Illuminate\Support\ServiceProvider;
use TTools\App as TTools;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(SerialDriver::class, function($app) {
           return new SerialDriver(config('app.serialPort'), config('app.baudRate'));
        });

        $this->app->singleton(TTools::class, function($app) {
           return new TTools(config('app.ttools'));
        });
    }
}
