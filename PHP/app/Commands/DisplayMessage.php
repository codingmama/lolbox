<?php
/**
 * Sends a static message to be displayed in the LedBox
 */
namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use Heidilabs\SerialDriver;

class DisplayMessage extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'display:message {message}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Displays a static message on the LedBox.';

    private $serial;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $message = $this->argument('message');

        $this->comment("Sending message: $message");

        /** @var SerialDriver $serial */
        $serial = app(SerialDriver::class);
        $serial->sendMessage($message);
        $serial->close();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
