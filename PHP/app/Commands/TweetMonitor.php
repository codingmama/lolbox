<?php

namespace App\Commands;

use Heidilabs\SerialDriver;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use TTools\App as TTools;

class TweetMonitor extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'tweet:monitor {tweetId?}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Uses the Twitter API to display info about a specific tweet (likes and RTs)';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        /** @var TTools $ttools */
        $ttools = app(TTools::class);
        /** @var SerialDriver $serial */
        $serial = app(SerialDriver::class);
        $tweetId = $this->argument('tweetId');

        if (!$tweetId) {
            $credentials = $ttools->getCredentials();
            $userinfo = $ttools->get('/users/show.json', [ 'screen_name' => $credentials['screen_name'] ]);
            $tweetId = $userinfo['status']['id_str'];
        }

        //this will run as a loop to fetch tweet every 30s
        while (true) {
            $this->comment("Fetching tweet info for tweet $tweetId...");
            $info = $ttools->get('/statuses/show.json', [ 'id' => $tweetId ]);
            $tweet = $info['text'];
            $rts = $info['retweet_count'];
            $favs = $info['favorite_count'];

            $this->info($tweet);
            $message = "Stats: $rts RTs, $favs Likes";

            $this->comment($message);
            $serial->sendMessage($message);
            $this->comment("Now sleeping...");
            sleep(30);
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
