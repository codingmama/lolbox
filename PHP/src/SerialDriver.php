<?php

/**
 * Sends messages via Serial
 */

namespace Heidilabs;

class SerialDriver
{
    protected $port;
    protected $baudRate;
    protected $serial;
    protected $delimiter;

    public function __construct($port, $baudRate = 19200, $delimiter = "\n")
    {
        $this->port = $port;
        $this->baudRate = $baudRate;
        $this->delimiter = $delimiter;
    }

    private function getSerial()
    {
        if ($this->serial == null) {
            $serial = new \PhpSerial();

            try {
                $serial->deviceSet("/dev/ttyUSB0");
                $serial->confBaudRate(19200);
                $serial->confParity("none");
                $serial->confCharacterLength(8);
                $serial->confStopBits(1);
                $serial->confFlowControl("none");
                $serial->deviceOpen();

                //wait a couple seconds to initialize device
                sleep(2);
            } catch (\Exception $e) {
                throw new SerialException("An error occurred while trying to open the Serial Port $this->port.
                Are you sure it's connected?");
            }

            $this->serial = $serial;
        }

        return $this->serial;
    }

    /**
     * Commands are 3-letter strings that will be sent along with a payload.
     * At the receiving end, you should look for the first 3 characters of the serial input in order to
     * identify the command. ex: "MSGthis is the message payload"
     * @param $command
     * @param $payload
     */
    public function sendCommand($command, $payload)
    {
        $this->getSerial()->sendMessage(strtoupper(substr($command, 0, 3)) . $payload . $this->delimiter);
    }

    /**
     * Sends a simple message / string without prefix
     * @param $string
     * @throws SerialException
     */
    public function sendMessage($string)
    {
        $this->getSerial()->sendMessage($string . $this->delimiter);
    }

    public function close()
    {
        if ($this->serial !== null) {
            $this->serial->deviceClose();
        }
    }
}