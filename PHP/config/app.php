<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */
    'name' => 'lolbox',

    /*
    |--------------------------------------------------------------------------
    | Application Version
    |--------------------------------------------------------------------------
    |
    | This value determines the "version" your application is currently running
    | in. You may want to follow the "Semantic Versioning" - Given a version
    | number MAJOR.MINOR.PATCH when an update happens: https://semver.org.
    |
    */
    'version' => app('git.version'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Should be true in production.
    |
    */
    'production' => false,

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */
    'providers' => [
        App\Providers\AppServiceProvider::class,
    ],

    /*
     * Serial Device Settings
     */
    'serialPort' => '/dev/ttyUSB0',

    'baudRate' => 19200,

    /*
     * Twitter API (TTools lib) Settings
     */
    'ttools' => [
        'consumer_key'        => 'lgeljXiueyOLElkT4reEwA',
        'consumer_secret'     => 'SkppBLCv652ycImVRnojAKwyy2rJj1gnqGgo4hBtfI',
        'access_token'        => '19625601-UYz2j7eAeMh1hxNRA2KMLi2s6yQvxXg1oA1BhISqQ',
        'access_token_secret' => 'f1BnsHhER6vuTb1IAsjJuPALEfvppJViG5eM6m7QNs0DB',
    ],
];
