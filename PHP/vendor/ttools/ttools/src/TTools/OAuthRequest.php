<?php
/**
 * OAuthRequest Class
 */

namespace TTools;

class OAuthRequest
{

    /** @var string Application consumer key */
    protected $consumerKey;

    /** @var string Application consumer secret */
    protected $consumerSecret;

    /** @var string Request Token */
    protected $token;

    /** @var string Request Secret */
    protected $tokenSecret;

    /** @var string Curl User Agent */
    protected $userAgent;

    /** @var  string */
    protected $boundary;

    /** @var  array */
    protected $headers;

    /** @var  string */
    protected $requestBody;

    const OAUTH_VERSION = '1.0';
    const OAUTH_SIGNATURE_METHOD = 'HMAC-SHA1';

    /**
     * Creates the OAuthRequest object
     *
     * @param string $consumerKey    Application Consumer Key
     * @param string $consumerSecret Application Consumer Secret
     * @param string $token          Request Token
     * @param string $tokenSecret    Request Token Secret
     */
    public function __construct($consumerKey, $consumerSecret, $token, $tokenSecret)
    {
        $this->userAgent      = 'TTools 2.0';

        $this->consumerKey    = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->token          = $token;
        $this->tokenSecret    = $tokenSecret;
        $this->boundary       = "ttools-" . md5(time());
    }

    /**
     * @param string $consumerKey
     */
    public function setConsumerKey($consumerKey)
    {
        $this->consumerKey = $consumerKey;
    }

    /**
     * @return string
     */
    public function getConsumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * @param string $consumerSecret
     */
    public function setConsumerSecret($consumerSecret)
    {
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->consumerSecret;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $tokenSecret
     */
    public function setTokenSecret($tokenSecret)
    {
        $this->tokenSecret = $tokenSecret;
    }

    /**
     * @return string
     */
    public function getTokenSecret()
    {
        return $this->tokenSecret;
    }

    /**
     * @param string $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Performs an authenticated OAuth Request
     *
     * @param string $method    Request Method (GET / POST)
     * @param string $url       Request endpoint (e.g: /1.1/account/verify_credentials.json)
     * @param array  $params    Params for the request
     * @param bool   $multipart If the request is multipart or not (defaults to false)
     *
     * @return OAuthResponse    Returns an OAuthResponse object
     */
    public function request($method, $url, $params = [], $multipart = false)
    {
        return $this->curlRequest($url, $params, $method, $multipart);
    }

    public function buildHeaders($method, $url, $params = [], $multipart = false)
    {
        if ($multipart) {
            $this->addHeader('Content-Type: multipart/form-data; boundary="' . $this->boundary . '"');
            $this->addHeader('Content-Length: ' . strlen($this->getRequestBody()));
            $this->addHeader('Expect: ');
        } else {
            $this->addHeader('Content-Type: application/x-www-form-urlencoded');
        }

        $this->addHeader('Authorization: ' . $this->getOAuthHeader($method, $url, $params, $multipart));
    }

    /**
     * Performs a OAuth curl request.
     *
     * @param string $url
     * @param array  $params
     * @param string $method
     * @param bool   $multipart
     *
     * @return OAuthResponse
     */
    protected function curlRequest($url, $params = [], $method = 'GET', $multipart = false)
    {
        $curl = curl_init();

        $requestUrl = $url;

        if ($method == 'GET') {
            $requestUrl = $url . '?' . $this->formatQueryString($params);
        } else {
            curl_setopt($curl, CURLOPT_POST, true);

            if ($multipart) {
                $data = $this->buildMultipartContent($params['media_file']);
                $this->setRequestBody($data);

            } else {
                $this->setRequestBody($this->formatQueryString($params));
            }

            curl_setopt($curl, CURLOPT_POSTFIELDS, $this->getRequestBody());
        }

        $this->buildHeaders($method, $url, $params, $multipart);

        curl_setopt_array($curl, [
            CURLOPT_USERAGENT      => $this->userAgent,
            CURLOPT_CONNECTTIMEOUT => 60,
            CURLOPT_TIMEOUT        => 20,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $requestUrl,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HEADER         => false,
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_HTTPHEADER     => $this->getHeaders(),
        ]);

        $response = new OAuthResponse();

        $response->setResponse(curl_exec($curl));
        $response->setCode(curl_getinfo($curl, CURLINFO_HTTP_CODE));
        $response->setInfo(curl_getinfo($curl));
        $response->setError(curl_error($curl));
        $response->setErrno(curl_errno($curl));

        curl_close($curl);

        return $response;
    }

    protected function buildMultipartContent($file)
    {
        $data = '--' . $this->boundary . "\r\n";
        $data .= 'Content-Disposition: form-data; name="media"; filename="' . basename($file) . '"' . "\r\n";
        $data .= 'Content-Type: application/octet-stream' . "\r\n";
        $data .= "\r\n" . file_get_contents($file) . "\r\n";
        $data .= '--' . $this->boundary . '--' . "\r\n";

        return $data;
    }

    /**
     * Gets the OAuth signed Authorization headers
     * @param string $method
     * @param string $url
     * @param array  $params
     * @param bool   $multipart
     *
     * @return string
     */
    protected function getOAuthHeader($method, $url, $params = [], $multipart = false)
    {
        $oauth['oauth_consumer_key']     = $this->consumerKey;
        $oauth['oauth_nonce']            = time();
        $oauth['oauth_signature_method'] = self::OAUTH_SIGNATURE_METHOD;
        $oauth['oauth_timestamp']        = time();
        $oauth['oauth_token']            = $this->token;
        $oauth['oauth_version']          = self::OAUTH_VERSION;

        if (!$multipart) {
            $signParams = array_merge($params, $oauth);
        } else {
            $signParams = $oauth;
        }

        uksort($signParams, 'strcmp');

        $query = $this->encodeParams($signParams);

        $queryString = implode('&', $query);

        $signature = strtoupper($method)
            . '&' . $this->urlencodeRfc3986($url)
            . '&' . $this->urlencodeRfc3986($queryString);

        $signingKey = $this->urlencodeRfc3986($this->consumerSecret)
            . '&' . $this->urlencodeRfc3986($this->tokenSecret);

        $hash_hmac = hash_hmac('sha1', $signature, $signingKey, true);
        $oauth['oauth_signature'] = base64_encode($hash_hmac);

        uksort($oauth, 'strcmp');
        $authParams = $this->encodeParams($oauth, true);

        return 'OAuth ' . implode(', ', $authParams);
    }

    /**
     * Encodes parameters
     * @param array $params
     * @param bool $quoted
     * @return array
     */
    protected function encodeParams(array $params = [], $quoted = false)
    {
        $encoded = [];

        foreach ($params as $key => $value) {
            if ($quoted) {
                $encodedValue = '"' . $this->urlencodeRfc3986($value) . '"';
            } else {
                $encodedValue = $this->urlencodeRfc3986($value);
            }
            $encoded[] = $this->urlencodeRfc3986($key) . '=' . $encodedValue;
        }

        return $encoded;
    }

    /**
     * @param $input
     * @return array|mixed|string
     */
    protected function urlencodeRfc3986($input)
    {
        if (is_array($input)) {
            return array_map(['TTools\OAuthRequest', 'urlencodeRfc3986'], $input);
        } elseif (is_scalar($input)) {
            return str_replace('+', ' ', str_replace('%7E', '~', rawurlencode($input)));
        } else {
            return '';
        }
    }

    /**
     * @param array $params
     * @return string
     */
    protected function formatQueryString(array $params)
    {
        if (!$params) {
            return '';
        }

        $keys = $this->urlencodeRfc3986(array_keys($params));
        $values = $this->urlencodeRfc3986(array_values($params));
        $params = array_combine($keys, $values);

        uksort($params, 'strcmp');

        $pairs = [];
        foreach ($params as $parameter => $value) {
            if (is_array($value)) {
                sort($value, SORT_STRING);
                foreach ($value as $duplicate_value) {
                    $pairs[] = $parameter . '=' . $duplicate_value;
                }
            } else {
                $pairs[] = $parameter . '=' . $value;
            }
        }
        return implode('&', $pairs);
    }

    /**
     * @return mixed
     */
    public function getRequestBody()
    {
        return $this->requestBody;
    }

    /**
     * @param mixed $requestBody
     */
    public function setRequestBody($requestBody)
    {
        $this->requestBody = $requestBody;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string $header
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }
}
